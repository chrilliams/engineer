json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :start, :comp
  json.url appointment_url(appointment, format: :json)
end
