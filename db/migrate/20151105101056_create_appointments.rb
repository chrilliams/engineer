class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :start

      t.timestamps null: false
    end
  end
end
